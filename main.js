import Vue from 'vue'
import App from './App'
import store from './store'
import * as config from './config'

Vue.prototype.$store = store
Vue.config.productionTip = false
const baseUrl=config.def().baseUrl
Vue.prototype.toLogin=function(backpage,backtype){
	uni.navigateTo({
		url:'/pages/login/login?backpage='+backpage+'&backtype='+backtype
	})
}
Vue.prototype.loginBack=function(backpage,backtype){
	let url='/pages/'+backpage+'/'+backpage
	if(backtype==0){
		uni.switchTab({
			url:url
		})
	}else if(backtype==1){
		uni.navigateTo({
			url:url
		})
	}else{
		uni.redirectTo({
			url:url
		})
	}
}
Vue.prototype.request=function(param,token){
	let netWork=''
	uni.getNetworkType({
		success:function(res){
			netWork=res.networkType
		}
	})
	if(netWork==='none'){
		uni.showToast({
			title:'当前无网络',
			icon:'none',
			duration:1500
		})
		return;
	}
	return new Promise(resolve => {
		let header={
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		}
		if(token!=null){
			header={
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
				'RIDERTOKEN': token
			}
		}
		uni.request({
			url:baseUrl+'/m.api',
			method:'POST',
			header:header,
			data:param,
			success:function(res) {
				// console.log(res)
				if(res.statusCode===200){  
					if(res.data.errno===200){
						resolve(res.data)
					}else if(res.data.errno===10001){
						uni.showModal({
							title: '系统提示',
							content: '您尚未登录，是否立即登录？',
							showCancel: true,
							confirmText: '取消',
							cancelText:'登录',
							success:function(res){
								if(res.cancel){
									uni.navigateTo({
										url:'/pages/login/login'
									})
								}
							}
						})
					}else{	
						uni.showToast({
							title:res.data.errmsg,
							icon:'none',
							duration:1500
						})
					}
				}
			},
			fail:function(err){
				uni.showToast({
					title:'请稍后再试',
					icon:'none'
				})
			}
			
		})
	})
	
}
Vue.prototype.new_request=function(param,token){
	let netWork=''
	uni.getNetworkType({
		success:function(res){
			netWork=res.networkType
		}
	})
	if(netWork==='none'){
		uni.showToast({
			title:'当前无网络',
			icon:'none',
			duration:1500
		})
		return;
	}
	return new Promise(resolve => {
		let header={
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		}
		if(token!=null){
			header={
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
				'RIDERTOKEN': token
			}
		}
		uni.request({
			url:baseUrl+'/m.api',
			method:'POST',
			header:header,
			data:param,
			success:function(res) {
				// console.log(res)
				if(res.statusCode===200){  
					if(res.data.errno===200){
						resolve(res.data)
					}
				}
			},
			fail:function(err){
				uni.showToast({
					title:'请稍后再试',
					icon:'none'
				})
			}
			
		})
	})
	
}
Vue.prototype.dateFtt=function(fmt,date)   
{    
  var o = {   
    "M+" : date.getMonth()+1,                 //月份   
    "d+" : date.getDate(),                    //日   
    "h+" : date.getHours(),                   //小时   
    "m+" : date.getMinutes(),                 //分   
    "s+" : date.getSeconds(),                 //秒   
    "q+" : Math.floor((date.getMonth()+3)/3), //季度   
    "S"  : date.getMilliseconds()             //毫秒   
  };   
  if(/(y+)/.test(fmt))   
    fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));   
  for(var k in o)   
    if(new RegExp("("+ k +")").test(fmt))   
  fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
  return fmt;   
}
App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
