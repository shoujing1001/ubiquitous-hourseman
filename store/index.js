
import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
const store = new Vuex.Store({  
    state: {   
        userInfo:uni.getStorageSync("userInfo")
    },  
    mutations: { 
        login(state, provider) {  
            console.log(state)  
            console.log(provider)  
			uni.setStorageSync('userInfo', provider)
        },  
        logout(state) {  
            state.hasLogin = false;  
            state.userInfo = '';
            uni.removeStorageSync('userInfo')
        }  
    }  
})
export default store